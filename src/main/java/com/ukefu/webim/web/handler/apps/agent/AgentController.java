package com.ukefu.webim.web.handler.apps.agent;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.webim.service.acd.ServiceQuene;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.repository.AgentServiceRepository;
import com.ukefu.webim.service.repository.AgentStatusRepository;
import com.ukefu.webim.service.repository.AgentUserRepository;
import com.ukefu.webim.service.repository.AgentUserTaskRepository;
import com.ukefu.webim.service.repository.BlackListRepository;
import com.ukefu.webim.service.repository.ChatMessageRepository;
import com.ukefu.webim.service.repository.OnlineUserRepository;
import com.ukefu.webim.service.repository.QuickReplyRepository;
import com.ukefu.webim.service.repository.TagRelationRepository;
import com.ukefu.webim.service.repository.TagRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.AgentStatus;
import com.ukefu.webim.web.model.AgentUser;
import com.ukefu.webim.web.model.AgentUserTask;
import com.ukefu.webim.web.model.BlackEntity;
import com.ukefu.webim.web.model.OnlineUser;
import com.ukefu.webim.web.model.SessionConfig;
import com.ukefu.webim.web.model.TagRelation;
import com.ukefu.webim.web.model.User;

@Controller
@RequestMapping("/agent")
public class AgentController extends Handler {
	
	@Autowired
	private AgentUserRepository agentUserRepository ;
	
	@Autowired
	private AgentStatusRepository agentStatusRepository ;
	
	@Autowired
	private AgentServiceRepository agentServiceRepository;
	
	@Autowired
	private OnlineUserRepository onlineUserRes;
	
	@Autowired
	private ChatMessageRepository chatMessageRepository ;
	
	@Autowired 
	private BlackListRepository blackListRes ;
	
	@Autowired
	private TagRepository tagRes ;
	
	@Autowired
	private TagRelationRepository tagRelationRes ;
	
	@Autowired
	private QuickReplyRepository quickReplyRes ;
	
	@Autowired
	private AgentUserTaskRepository agentUserTaskRes ;
	
	@RequestMapping("/index")
	@Menu(type = "apps", subtype = "agent")
	public ModelAndView index(HttpServletRequest request) {
		ModelAndView view = request(super.createAppsTempletResponse("/apps/agent/index")) ; 
		User user = super.getUser(request) ;
		List<AgentUser> agentUserList = agentUserRepository.findByAgentno(user.getId());
		view.addObject("agentUserList", agentUserList) ;
		
		if(agentUserList.size() > 0){
			AgentUser agentUser = agentUserList.get(0) ;
			agentUser = (AgentUser) agentUserList.get(0);
			view.addObject("curagentuser", agentUser);

			view.addObject("agentUserMessageList", this.chatMessageRepository.findBySession(agentUser.getUserid() , new PageRequest(0, 20, Direction.DESC , "createtime")));
			OnlineUser onlineUser = this.onlineUserRes.findByUserid(agentUser.getUserid()) ;
			if(UKDataContext.OnlineUserOperatorStatus.OFFLINE.toString().equals(onlineUser.getStatus())){
				onlineUser.setBetweentime((int) (onlineUser.getUpdatetime().getTime() - onlineUser.getLogintime().getTime()));
			}else{
				onlineUser.setBetweentime((int) (System.currentTimeMillis() - onlineUser.getLogintime().getTime()));
			}
			view.addObject("onlineUser",onlineUser);

			view.addObject("serviceCount", Integer
					.valueOf(this.agentServiceRepository
							.countByUseridAndOrgiAndStatus(agentUser
									.getUserid(), super.getOrgi(request),
									UKDataContext.AgentUserStatusEnum.END.toString())));
			
			view.addObject("tags", tagRes.findByOrgi(super.getOrgi(request))) ;
			view.addObject("tagRelationList", tagRelationRes.findByDataid(onlineUser.getId())) ;
			view.addObject("quickReplyList", quickReplyRes.findByOrgi(super.getOrgi(request))) ;
		}
		return view ;
	}
	
	@RequestMapping("/agentusers")
	@Menu(type = "apps", subtype = "agent")
	public ModelAndView agentusers(HttpServletRequest request , String userid) {
		ModelAndView view = request(super.createRequestPageTempletResponse("/apps/agent/agentusers")) ;
		User user = super.getUser(request) ;
		view.addObject("agentUserList", agentUserRepository.findByAgentno(user.getId())) ;
		view.addObject("curagentuser", agentUserRepository.findByUserid(userid)) ;
		return view ;
	}
	
	@RequestMapping("/agentuser")
	@Menu(type = "apps", subtype = "agent")
	public ModelAndView agentuser(HttpServletRequest request , String id) {
		ModelAndView view = request(super.createRequestPageTempletResponse("/apps/agent/mainagentuser")) ;
		AgentUser agentUser = agentUserRepository.findById(id);
		view.addObject("curagentuser", agentUser) ;
		
		AgentUserTask agentUserTask = agentUserTaskRes.findById(id) ;
		agentUserTask.setTokenum(0);
		agentUserTaskRes.save(agentUserTask) ;
		
		
		view.addObject("agentUserMessageList", this.chatMessageRepository.findBySession(agentUser.getUserid() , new PageRequest(0, 20, Direction.DESC , "createtime")));
		
		OnlineUser onlineUser = this.onlineUserRes.findByUserid(agentUser.getUserid()) ;
		if(UKDataContext.OnlineUserOperatorStatus.OFFLINE.toString().equals(onlineUser.getStatus())){
			onlineUser.setBetweentime((int) (onlineUser.getUpdatetime().getTime() - onlineUser.getLogintime().getTime()));
		}else{
			onlineUser.setBetweentime((int) (System.currentTimeMillis() - onlineUser.getLogintime().getTime()));
		}
		view.addObject("onlineUser",onlineUser);

		view.addObject("serviceCount", Integer
				.valueOf(this.agentServiceRepository
						.countByUseridAndOrgiAndStatus(agentUser
								.getUserid(), super.getOrgi(request),
								UKDataContext.AgentUserStatusEnum.END
										.toString())));
		
		view.addObject("tags", tagRes.findByOrgi(super.getOrgi(request))) ;
		view.addObject("tagRelationList", tagRelationRes.findByDataid(onlineUser.getId())) ;
		view.addObject("quickReplyList", quickReplyRes.findByOrgi(super.getOrgi(request))) ;

		return view ;
	}
	
	@RequestMapping(value="/ready")  
	@Menu(type = "apps", subtype = "agent")
    public ModelAndView ready(HttpServletRequest request){ 
		User user = super.getUser(request) ;
    	AgentStatus agentStatus = agentStatusRepository.findByAgentno(user.getId());
    	if(agentStatus==null){
    		agentStatus = new AgentStatus() ;
	    	agentStatus.setUserid(user.getId());
	    	agentStatus.setUsername(user.getUsername());
	    	agentStatus.setAgentno(user.getId());
	    	agentStatus.setLogindate(new Date());
	    	SessionConfig sessionConfig = ServiceQuene.initSessionConfig(super.getOrgi(request)) ;
	    	agentStatus.setOrgi(user.getOrgi());
	    	agentStatus.setMaxusers(sessionConfig.getMaxuser());
	    	agentStatusRepository.save(agentStatus) ;
    	}
    	
    	agentStatus.setStatus(UKDataContext.AgentStatusEnum.READY.toString());
    	CacheHelper.getAgentStatusCacheBean().put(agentStatus.getAgentno(), agentStatus, user.getOrgi());
    	
    	ServiceQuene.allotAgent(agentStatus, user.getOrgi());
    	
    	return request(super.createAppsTempletResponse("/public/success")) ; 
    }
	
	@RequestMapping(value="/notready") 
	@Menu(type = "apps", subtype = "agent")
    public ModelAndView notready(HttpServletRequest request){ 
		User user = super.getUser(request) ;
		AgentStatus agentStatus = agentStatusRepository.findByAgentno(user.getId());
		if(agentStatus!=null){
			agentStatusRepository.delete(agentStatus);
		}
    	CacheHelper.getAgentStatusCacheBean().delete(super.getUser(request).getId(), user.getOrgi());;
    	ServiceQuene.publishMessage(user.getOrgi());
    	
    	return request(super.createAppsTempletResponse("/public/success")) ; 
    }
	
	@RequestMapping({ "/end" })
	@Menu(type = "apps", subtype = "agent")
	public ModelAndView end(HttpServletRequest request, @Valid String userid)
			throws Exception {
		User user = super.getUser(request);
		AgentUser agentUser = agentUserRepository.findById(userid);
		if(agentUser!=null){
			ServiceQuene.deleteAgentUser(agentUser, user.getOrgi());
		}
		return request(super
				.createRequestPageTempletResponse("redirect:/agent/index.html"));
	}
	
	@RequestMapping({ "/readmsg" })
	@Menu(type = "apps", subtype = "agent")
	public ModelAndView readmsg(HttpServletRequest request, @Valid String userid)
			throws Exception {
		AgentUserTask agentUserTask = agentUserTaskRes.findById(userid);
		if(agentUserTask!=null){
			agentUserTask.setTokenum(0);
			agentUserTaskRes.save(agentUserTask);
		}
		return request(super.createRequestPageTempletResponse("/public/success"));
	}
	
	@RequestMapping({ "/blacklist" })
	@Menu(type = "apps", subtype = "blacklist")
	public ModelAndView blacklist(HttpServletRequest request, @Valid String agentuserid , @Valid String agentserviceid ,  @Valid String userid , @Valid BlackEntity blackEntity)
			throws Exception {
		User user = super.getUser(request);
		OnlineUser onlineUser = onlineUserRes.findByUserid(userid) ;
		BlackEntity tempBlackEntiry = blackListRes.findByUserid(onlineUser.getUserid()) ;
		if(onlineUser!=null && tempBlackEntiry == null){
			blackEntity.setUserid(userid);
			blackEntity.setCreater(user.getId());
			blackEntity.setOrgi(super.getOrgi(request));
			blackEntity.setAgentid(user.getId());
			blackEntity.setSessionid(onlineUser.getSessionid());
			blackEntity.setAgentserviceid(agentserviceid);
			blackEntity.setChannel(onlineUser.getChannel());
			blackListRes.save(blackEntity) ;
		}
		return end(request , agentuserid);
	}
	
	@RequestMapping("/tagrelation")
	@Menu(type = "apps", subtype = "tagrelation")
    public ModelAndView blacklistdelete(ModelMap map , HttpServletRequest request , @Valid String userid , @Valid String tagid,@Valid String dataid) {
		TagRelation tagRelation = tagRelationRes.findByDataidAndTagid(dataid, tagid) ;
		if(tagRelation==null){
			tagRelation = new TagRelation();
			tagRelation.setUserid(userid);
			tagRelation.setTagid(tagid);
			tagRelation.setDataid(dataid);
			tagRelationRes.save(tagRelation) ;
		}else{
			tagRelationRes.delete(tagRelation);
		}
		return request(super
				.createRequestPageTempletResponse("/public/success"));
    }
}